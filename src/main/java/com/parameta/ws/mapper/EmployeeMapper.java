package com.parameta.ws.mapper;

import com.parameta.ws.model.Employee;
import com.parameta.ws.model.EmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmployeeMapper {
    EmployeeMapper MAPPER = Mappers.getMapper( EmployeeMapper.class );

    Employee toEmployee( EmployeeDTO userDto );

}
