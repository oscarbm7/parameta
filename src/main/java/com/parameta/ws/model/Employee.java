package com.parameta.ws.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "EMPLOYEE")
public class Employee {
    
    private static final long serialVersionUID = -2038885120192288282L;
    
    @Id
    @Column(name = "id")
    @NotNull
    private long id;    

    @Column(name = "name")
    private String name;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "document_id")
    private String documentId;
    
    @Column(name = "document_type")
    private String documentType;
    
    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "hiredate")
    private Date hiredate;
    
    @Column(name = "position")
    private String position;
    
    @Column(name = "salary")
    private Integer salary;
    
    public void setBirthdate(Date birthdate) {
        this.birthdate = new Date(birthdate.getTime());
    }

    public Date getBirthdate() {
        return new Date(birthdate.getTime());
    }

    public void setHiredate(Date hiredate) {
        this.hiredate = new Date(hiredate.getTime());
    }
    
    public Date getHiredate() {
        return new Date(hiredate.getTime());
    }
}
