package com.parameta.ws.services.repository;

import com.parameta.ws.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Override
    Employee save(Employee user);
}
