package com.parameta.ws.services;

import com.parameta.ws.model.EmployeeDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(serviceName = "UserService")
public interface EmployeeService {

    @WebMethod()
    @WebResult(name = "Greeting")
    public EmployeeDTO registerEmployee(@WebParam(name = "UserRequest") EmployeeDTO employeeDTO);

}
