package com.parameta.ws;



import static com.parameta.ws.constants.GeneralConstants.GLOBAL_BASE_PACKAGE;

import lombok.extern.log4j.Log4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = GLOBAL_BASE_PACKAGE)
@EntityScan(basePackages = GLOBAL_BASE_PACKAGE)
@ComponentScan(basePackages = GLOBAL_BASE_PACKAGE)
@ImportResource({ "classpath:webservice-definition-beans.xml" })
@EnableAsync
@Log4j
public class EmployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeApplication.class, args);
    }
    
}
