package com.parameta.ws.services;

import com.parameta.ws.model.Employee;
import com.parameta.ws.model.EmployeeDTO;
import com.parameta.ws.services.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.parameta.ws.mapper.EmployeeMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository  employeeRepository;
    
    @Override
    public EmployeeDTO registerEmployee(EmployeeDTO userDTO) {
        log.info("Creating employee..");
        Employee employee = EmployeeMapper.MAPPER.toEmployee(userDTO);
        employeeRepository.save(employee);      
        
        return userDTO;
    }

}
